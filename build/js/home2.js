$(document).ready(function () {

    $.ajaxSetup({
        xhrFields: {
            withCredentials: true
        }
    });

    $.ajax({
        url: localStorage.baseURL+"/account-info",
        type: "GET",
        success: getName
    });

    function getName(query) {
        var name;
        name = query.data.accountInfo.name;
        document.getElementById("welcome").innerHTML = "Welcome " + name.toUpperCase();
    }

});