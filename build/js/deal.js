let vouchersURL = localStorage.baseURL+"/vouchers";
var vouchersTable;
var vouchers = [];

$(document).ready(function () {

    vouchersTable = $('#vouchersTable').DataTable({
        "language": {
            "emptyTable": "No data available in the table"
        },
        "pageLength": 50,
        "bLengthChange": false,
        "autoWidth": false,
        "pagingType": "simple",
        "columns": [
            {"bVisible": false, "targets": 0},
            {"width": "10%", "targets": 1},
            {"width": "10%", "targets": 2},
            {"width": "10%", "targets": 3},
            {"width": "15%", "targets": 4},
            {"width": "20%", "targets": 5},
            {"width": "10%", "targets": 6},
            {"width": "10%", "targets": 7},
            {"width": "10%", "targets": 8},
            {
                "data": null,
                "defaultContent": "<button id=\"deleteBtn\" type=\"button\" class=\"btn green-1\" style='max-width: 8%; background-color: darkgrey !important;'> Delete </button>",
                "target": 9
            }
        ]
    });

    //Listener on Delete Button
    $("#vouchersTable tbody").on('click', '.btn', function() {
        //Get the barcode of the voucher
        var actualDisplayedData = vouchersTable.row($(this).parents('tr')).data();
        var voucherToDeleteID;

        //By visiting each voucher, get the ID of the voucher which corrspond to the barcode
        for(var i = 0; i < vouchers.length; i++){
            if(vouchers[i].barcode === actualDisplayedData[2]){
                voucherToDeleteID = vouchers[i].id;
            }
        }
        //Delete this voucher
        deleteVoucher(voucherToDeleteID);
    });

    $.ajaxSetup({
        xhrFields: {
            withCredentials: true
        }
    });

    $("#import").click(function () {
        $("#importBtn").css("display", "none");
        $("#choseFile").css("display", "block");
    })

    //Get the vouchers when the document is ready
    getVouchers();
});

//Function that fill the table with the date from the API Response
function fillTable(Query) {

    data = Query.data.vouchers;data
    localStorage.setItem('data', JSON.stringify(data));

    nextUrl = Query.data.meta.nextPage;
    previousUrl = Query.data.meta.currentPage;

    if (Query.data.meta.hasOwnProperty("totalPages")){
        totalPages = Query.data.meta.totalPages;
        current = Query.data.meta.currentPage;
    }
    else {
        totalPages = 1;
        current = 1;
    }

    currentUrl = vouchersURL;
    previousUrl = updateQueryStringParameter(vouchersURL, "page", current-1);

    if(current < totalPages){
        $.ajax({
            url: updateQueryStringParameter(vouchersURL, "page", current+1),
            type: "GET",
            success: fillTable,
            beforeSend: function(){
                $('#loaderContainer').show()
                $('#pageContainer').hide();
            },
            complete: function(){
                $('#loaderContainer').hide();
                $('#pageContainer').show();
            },
    
        });
    }

    for (var i in data) {
        
        vouchers.push(data[i]);

        var id = data[i].id;
        var barcode = data[i].barcode;
        var title = data[i].title;
        var description = data[i].description;
        var status = data[i].status;


        var expiryDate = data[i].expiryDate;
        var date = new Date(expiryDate*1000);
        expiryDate = date.toLocaleDateString();

        var creationDate = data[i].creationDate;
        date = new Date(creationDate*1000);
        creationDate = date.toLocaleDateString();

        //Create a empty Redeem date and QR Code as the API does not send such field
        var redeemDate = "";
        var qrCode = "";

        vouchersTable.row.add([" ",creationDate, barcode, status, title, description, expiryDate, redeemDate, qrCode]).draw();

        $("#currentPage").text(current);
        $("#totalPages").text(totalPages);
    }
}

//Listener on the Filter Button
$("#applyFilter").click(function() {
    //Clear the table
    vouchersTable.clear().draw();
   switch(parseInt($("#filterStatus").val())){
    case 1 :
        getActiveVouchers();
        break;
    case 2 :
        getExpiredVouchers();
        break;
    case 3 :
        getRedeemedVouchers();
        break;
    default:
        getVouchers();
        break;
   }
});

//Listener on Import to CSV Button
$("#downloadButton").click(function() {
    tableToCSV();
});

//Convert to CSV and make downloadable the table
function tableToCSV(){
    $("#vouchersTable").tableToCSV();
}

//Open csv file
var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
        var text = reader.result;
        var numberOfVouchers = text.split("\n").length-1;

        //Get the actual vouchersArray
        var receivedBarcode = [];
        var receivedExpiryDate = [];
        var receivedTitle = [];
        var receivedDescription = [];

        for(var i = 0; i < numberOfVouchers; i++){
           receivedBarcode.push(text.split("\n")[i].split(",")[0]);
           receivedExpiryDate.push(text.split("\n")[i].split(",")[1]);
           receivedTitle.push(text.split("\n")[i].split(",")[2]);
           receivedDescription.push(text.split("\n")[i].split(",")[3]);
        }

        //Reform this array to correspond to the expected format by the backend
        var vouchersArrayContent = []
        var voucherObject;

        for(var i = 0; i < numberOfVouchers; i++){
            voucherObject = {barcode: receivedBarcode[i], expiryDate: receivedExpiryDate[i], title: receivedTitle[i], description: receivedDescription[i]};
            vouchersArrayContent.push(voucherObject);
        }

        var vouchersArray = {"vouchers" : vouchersArrayContent}
        var vouchersArrayToJSON = JSON.stringify(vouchersArray);

        sendVouchers(vouchersArrayToJSON);

        //console.log(vouchersArrayToJSON);

    };
    reader.readAsText(input.files[0]);
};

 //Listener on Delete All active vouchers Button
 $("#deleteAllActiveVouchers").click(function() {
    deleteAllActiveVouchers();
});

//Function that replace the url with the next page url
function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}


//API Call to get all the vouchers
function getVouchers(){
    $.ajax({
        url: vouchersURL,
        type: "GET",
        success: fillTable,
        beforeSend: function(){
            $('#loaderContainer').show()
            $('#pageContainer').hide();
        },
        complete: function(){
            $('#loaderContainer').hide();
            $('#pageContainer').show();
        },

    });
}

//API Call to get the active vouchers
function getActiveVouchers(){
    $.ajax({
        url: vouchersURL + "?status=active",
        type: "GET",
        success: fillTable,
        beforeSend: function(){
            $('#loaderContainer').show()
            $('#pageContainer').hide();
        },
        complete: function(){
            $('#loaderContainer').hide();
            $('#pageContainer').show();
        },

    });
}

//API Call to get the expired vouchers
function getExpiredVouchers(){
    $.ajax({
        url: vouchersURL + "?status=expired",
        type: "GET",
        success: fillTable,
        beforeSend: function(){
            $('#loaderContainer').show()
            $('#pageContainer').hide();
        },
        complete: function(){
            $('#loaderContainer').hide();
            $('#pageContainer').show();
        },

    });
}

//API Call to get the redeemed vouchers
function getRedeemedVouchers(){
    $.ajax({
        url: vouchersURL + "?status=redeemed",
        type: "GET",
        success: fillTable,
        beforeSend: function(){
            $('#loaderContainer').show()
            $('#pageContainer').hide();
        },
        complete: function(){
            $('#loaderContainer').hide();
            $('#pageContainer').show();
        },

    });
}

//API call to send the vouchers
function sendVouchers(vouchersArray) {
    $.ajax({
        contentType: 'application/json',
        url: vouchersURL,
        data: vouchersArray,
        type: "POST",
        success: function(){
            location.reload();
        },
        error: function(data){
           alert(data.responseJSON.error.message);
           location.reload();
        }
    })
}

//API call to delete the voucher
function deleteVoucher(voucherIdParameter) {
    if(confirm("Are you sure you want to delete this voucher ?")){
        $.ajax({
            url: vouchersURL+"/"+voucherIdParameter,
            type: "DELETE",
            complete: function(){
                location.reload();
            },
        })
    }
}

//API call to delete all the vouchers
function deleteAllActiveVouchers() {
    if(confirm("Are you sure you want to delete all the active vouchers ?")){
        $.ajax({
            url: vouchersURL,
            type: "DELETE",
            complete: function(){
                location.reload();
            },
        })
    }
}
