
$(document).ready(function() {

    $("#buttonSignIn").click(function (e) {

        $("#warningBoxSignIn").hide();

        if( ! $("#emailSignIn").val().includes("@")){

            $("#emailSignInWarning")
                .text("Please enter a valid email.")
                .show();

        }

        if( $("#passwordSignIn").val().length < 8) {

            $("#passwordSignInWarning")
                .text("Please enter a valid password.")
                .show();
        }

        if( $("#emailSignIn").val().includes("@") && $("#passwordSignIn").val().length >= 8 ){

            $.ajaxSetup({
                xhrFields: {
                    withCredentials: true
                }
            });

            let data = {
                "email": $("#emailSignIn").val(),
                "password": $("#passwordSignIn").val()
            };

            let stringData = JSON.stringify(data);

            e.preventDefault();

            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                type: 'POST',
                url: "http://13.251.207.57/api/dashboard/admin/signin",
                data: stringData,
                success: function () { window.location = './home.html'},
                error: function(jqXHR) {
                    $("#warningSignIn")
                        .val("Incorrect email or password");
                    $("#warningBoxSignIn").show();
                    $("#buttonSignIn").prop('disabled', false);
                    $("#loader").hide();
                    $("#signin-content").show();

                },
                beforeSend: function(){
                    $("#buttonSignIn").prop('disabled', true);
                    $("#loader").show();
                    $("#signin-content").hide();
                },
            });
        }

    });

    $('#emailSignIn').keyup(function(e){

        if (e.which === 13) {
            $('#passwordSignIn').focus();
        }
        else{
            if(!$(this).val().includes("@")){
                $("#emailSignInWarning").show();
                $("#warningBoxSignIn").hide();

            }else{
                $("#emailSignInWarning").hide();
                $("#warningBoxSignIn").hide();

            }
        }
    });

    $('#passwordSignIn').keyup(function(e){

        if (e.which === 13) {
            $('#buttonSignIn').focus()
                .trigger('click');
        }
        else{
            if($(this).val().length < 8){
                $("#passwordSignInWarning").show();
                $("#warningBoxSignIn").hide();
            }else{
                $("#passwordSignInWarning").hide();
                $("#warningBoxSignIn").hide();
            }
        }
    });


});