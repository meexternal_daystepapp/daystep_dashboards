jQuery.fn.tableToCSV = function() {
    
    var clean_text = function(text){
        text = text.replace(/"/g, '""');
        return '"'+text+'"';
    };
    
	$(this).each(function(){
			var table = $(this);
			var caption = $(this).find('caption').text();
			//var title = [];
			var rows = [];

			$(this).find('tr').each(function(){
				var data = [];
				/* $(this).find('th').each(function(){
					var text = clean_text($(this).text());
					if(title.length < 8){
						title.push(text);
					}
				}); */
				$(this).find('td').each(function(){
					var text = clean_text($(this).text());
					if(data.length < 8){
						data.push(text);
					}
				});
				data = data.join(",");
				rows.push(data);
				});
			//title = title.join(",");
			rows.shift();
			rows = rows.join("\n");

			//var csv = title + rows;
			var csv = rows;

			var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
			var download_link = document.createElement('a');
			download_link.href = uri;

			var currentPage = vouchersTable.page.info().page + 1;
			currentPage = currentPage.toString();

			var day = new Date().getDate().toString();
			if (day.length <2){
				day = "0" + day;
			}
			var month = new Date().getMonth() + 1;
			month = month.toString();
			if (month.length < 2){
				month = "0" + month;
			}
			var year = new Date().getFullYear().toString();

			var ts = "Daystep_Dashboard_" + day + "-" + month + "-" + year + "_Page_" + currentPage;

			download_link.download = ts+".csv";

			document.body.appendChild(download_link);
			download_link.click();
			document.body.removeChild(download_link);
	});
    
};
