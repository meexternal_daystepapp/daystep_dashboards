$(document).ready(function () {

    let usersTable = $('#usersTable').DataTable({
        "language": {
            "emptyTable": "No data available in the table"
        },
        "paging": false,
        "searching": false,
        "info": false,
        "autoWidth": false,
        "columnDefs": [
            // {"bVisible": false, "targets": 0},
            {"width": "25%", "targets": 0},
            {"width": "15%", "targets": 1},
            {"width": "20%", "targets": 2},
            {"width": "10%", "targets": 3},
            {"width": "15%", "targets": 4},
            {
                "data": null,
                "defaultContent": "<button id=\"showDetail\" type=\"button\" class=\"btn green-1\" style='max-width: 8%; background-color: darkgrey !important;'> Details </button>",
                "target": 5
            }
        ]
    });


    $.ajaxSetup({
        xhrFields: {
            withCredentials: true
        }
    });

});