$(document).ready(function () {

    let giftsTable = $('#giftsTable').DataTable({
        "language": {
            "emptyTable": "No data available in the table"
        },
        "paging": false,
        "searching": false,
        "info": false,
        "autoWidth": false,
        "columns": [
            {"bVisible": false, "targets": 0},
            {"width": "21%", "targets": 1},
            {"width": "22%", "targets": 2},
            {"width": "24%", "targets": 3},
            {"width": "21%", "targets": 4},
            {
                "data": null,
                "defaultContent": "<button id=\"win\" type=\"button\" class=\"btn green-1\" style='max-width: 8%; background-color: #E3F735 !important;'> Win </button>",
                "target": 5
            }
        ]
    });

    $.ajaxSetup({
        xhrFields: {
            withCredentials: true
        }
    });

    let nextUrl ;
    let currentUrl;
    let previousUrl;
    let current = 1;
    let totalPages = 1;
});

$(function() {
    $("#dateGiftInput").datepicker();
    });